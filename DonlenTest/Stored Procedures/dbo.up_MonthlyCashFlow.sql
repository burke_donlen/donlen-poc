SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create Procedure [dbo].[up_MonthlyCashFlow]
	@ReportPeriod DateTime2(0)
As

Insert Into dbo.MonthlyCashFlow ( AccountId, ReportPeriod, NetMonthlyCashFlow )
Select	d.AccountId
	,	@ReportPeriod
	,	Sum(d.NetCashFlow)
From dbo.DailyCashFlow d
Where DateAdd(Month, DateDiff(Month, 0, d.ReportPeriod), 0) = @ReportPeriod
Group By d.AccountId

GO
