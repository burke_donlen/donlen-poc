CREATE TABLE [dbo].[DailyCashFlow]
(
[DailyCashFlowId] [int] NOT NULL IDENTITY(1, 1),
[AccountId] [int] NOT NULL,
[ReportPeriod] [datetime2] (0) NOT NULL,
[NetCashFlow] [decimal] (11, 4) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DailyCashFlow] ADD CONSTRAINT [PK_DailyCashFlow] PRIMARY KEY CLUSTERED ([DailyCashFlowId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DailyCashFlow] ADD CONSTRAINT [FK_DailyCashFlow_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Accounts] ([AccountId])
GO
