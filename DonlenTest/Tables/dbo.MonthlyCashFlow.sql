CREATE TABLE [dbo].[MonthlyCashFlow]
(
[MonthlyCashFlowId] [int] NOT NULL IDENTITY(1, 1),
[AccountId] [int] NOT NULL,
[ReportPeriod] [datetime2] (0) NOT NULL,
[NetMonthlyCashFlow] [decimal] (11, 4) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MonthlyCashFlow] ADD CONSTRAINT [PK_MonthlyCashFlow] PRIMARY KEY CLUSTERED ([MonthlyCashFlowId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MonthlyCashFlow] ADD CONSTRAINT [FK_MonthlyCashFlow_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Accounts] ([AccountId])
GO
