SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure [dbo].[up_DailyCashFlow]
	@CashFlowDate DateTime2(0)
As

Create Table #CashFlowByDate (
	[CashFlowDate] DateTime2(0) Not Null,
	[AccountId] Int Not Null,
	[NetCashFlow] Decimal(11, 4) Not Null
)

Insert Into #CashFlowByDate ( CashFlowDate, AccountId, NetCashFlow )
Select	t.TransactionDate
	,	t.AccountId
	,	Sum(t.Amount)
From dbo.Transactions t
Group By t.AccountId,
		 t.TransactionDate

Insert Into dbo.DailyCashFlow ( AccountId, ReportPeriod, NetCashFlow )
Select	cf.AccountId
	,	cf.CashFlowDate
	,	cf.NetCashFlow
From #CashFlowByDate cf
Where cf.CashFlowDate = @CashFlowDate

GO
