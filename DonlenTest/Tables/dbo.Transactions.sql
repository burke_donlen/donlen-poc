CREATE TABLE [dbo].[Transactions]
(
[TransactionId] [bigint] NOT NULL IDENTITY(1, 1),
[TransactionDate] [datetime2] (0) NOT NULL,
[AccountId] [int] NOT NULL,
[Amount] [decimal] (9, 4) NOT NULL,
[TransactionDescription] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [PK_Transactions] PRIMARY KEY CLUSTERED ([TransactionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_Transactions_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Accounts] ([AccountId])
GO
