CREATE TABLE [dbo].[Accounts]
(
[AccountId] [int] NOT NULL IDENTITY(1, 1),
[AccountName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Accounts] ADD CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED ([AccountId]) ON [PRIMARY]
GO
